/* ESP8266 BLINKING CLOCK

   Each LED color blinks to indicate time of day

   Copyright(c) 2016, 2017 Jonathan Poland
*/
#include <EEPROM.h>
#include <ESP8266mDNS.h>
#include <ESP8266WiFi.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include <NTPClient.h>
#include <TaskScheduler.h>
#include <WiFiManager.h>
#include <Ticker.h>
#include <Time.h>
#include <TimeLib.h>
#include <Timezone.h> //https://github.com/JChristensen/Timezone
#include <ArduinoOTA.h>


#include "FS.h"

#define INPUT_LDR A0
#define INPUT_BUTTON 4
#define LED_TINY_BLU 2
#define LED_GRN 12
#define LED_BLU 13
#define LED_RED 15
#define EEPROM_SIZE 512
#define ULONG_MAX 4294967295

#define INO_VERSION 7

typedef enum {
  NONE,
  HOURS,
  TENS,
  MINS
} State;

typedef struct rgb {
  uint8_t r;
  uint8_t g;
  uint8_t b;
} RGB;

// What we write to and from EEPROM
typedef struct cfg {
  int offset;
  uint8_t r_brightness;
  uint8_t g_brightness;
  uint8_t b_brightness;
  char name[20];
} Config;

// Globals
Ticker ticker;
WiFiUDP ntpUDP;
ESP8266WebServer server(80);
NTPClient timeClient(ntpUDP, "pool.ntp.org");
Scheduler runner;
WiFiManager wifiManager;
Config config;
bool web_reset = false;

// Timezone Globals
//US Eastern Time Zone (New York, Detroit)
TimeChangeRule usEDT = {"EDT", Second, Sun, Mar, 2, -240};  
TimeChangeRule usEST = {"EST", First, Sun, Nov, 2, -300};   
Timezone usET(usEDT, usEST);

//US Central Time Zone (Chicago, Houston)
TimeChangeRule usCDT = {"CDT", Second, dowSunday, Mar, 2, -300};
TimeChangeRule usCST = {"CST", First, dowSunday, Nov, 2, -360};
Timezone usCT(usCDT, usCST);

//US Mountain Time Zone (Denver, Salt Lake City)
TimeChangeRule usMDT = {"MDT", Second, dowSunday, Mar, 2, -360};
TimeChangeRule usMST = {"MST", First, dowSunday, Nov, 2, -420};
Timezone usMT(usMDT, usMST);

//Arizona is US Mountain Time Zone but does not use DST
Timezone usAZ(usMST, usMST);

//US Pacific Time Zone (Las Vegas, Los Angeles)
TimeChangeRule usPDT = {"PDT", Second, dowSunday, Mar, 2, -420};
TimeChangeRule usPST = {"PST", First, dowSunday, Nov, 2, -480};
Timezone usPT(usPDT, usPST);
// Pointer to current TZ
Timezone *tzp = NULL;

// Used during setup to blink the LED
void tick() {
  static int color = LED_BLU;
  static int brightness = 0;
  if (brightness == 0) {
    brightness = map(analogRead(INPUT_LDR), 0, 1024, 1, 255);
  } else {
    brightness = 0;
    if (color == LED_BLU) {
      color = LED_RED;
    } else if (color == LED_RED) {
      color = LED_GRN;
    } else if (color == LED_GRN) {
      color = LED_BLU;  
    }
  }
  if (color == LED_BLU) {
    analogWrite(LED_GRN, 0);
    analogWrite(LED_BLU, brightness);
  } else if (color == LED_RED) {
    analogWrite(LED_BLU, 0);
    analogWrite(LED_RED, brightness);  
  } else if (color == LED_GRN) {
    analogWrite(LED_RED, 0);
    analogWrite(LED_GRN, brightness);  
  }
}

// Be a Wifi access point if we don't have any wifi config
void configModeCallback (WiFiManager *wifiManagerPtr) {
  Serial.println("Entered config mode");
  Serial.println(WiFi.softAPIP());
  //if you used auto generated SSID, print it
  Serial.println(wifiManagerPtr->getConfigPortalSSID());
  //entered config mode, make led toggle faster
  ticker.attach(0.1, tick);
}

void update_internal_offset(int offset) {
  if (offset == -14401) {
    offset = 0;
    tzp = &usET;
  } else if (offset == -18001) {
    offset = 0;
    tzp = &usCT;
  } else if (offset == -21601) {
    offset = 0;
    tzp = &usMT;
  } else if (offset == -25201) {
    offset = 0;
    tzp = &usPT;
  } else {
    tzp = NULL;
  }
  timeClient.setTimeOffset(offset);
}

// Set local clock via NTP
void time_init() {
  update_internal_offset(config.offset);
  timeClient.setUpdateInterval(900000);
  timeClient.begin();
  timeClient.update();
  Serial.print("Attempting to synchronize time..");
  while (ULONG_MAX - timeClient.getEpochTime() < abs(config.offset)) {
    timeClient.update();
    delay(1000);
    Serial.print(".");
  }
  if (tzp) {
    time_t tztime = tzp->toLocal(timeClient.getEpochTime());
    Serial.println(String(hour(tztime)) + ":" + minute(tztime));
  } else {
    Serial.println(timeClient.getFormattedTime());
  }
}

// Set up the network
void wifi_init() {
  wifiManager.setTimeout(300);
  wifiManager.setAPCallback(configModeCallback);
  if (!wifiManager.autoConnect()) {
    Serial.println("failed to connect and hit timeout");
    ESP.restart();
  }
}

void mdns_init() {
  const char * myname = strlen(config.name) ? config.name: "blinkclock";
  if (!MDNS.begin(myname)) {
    Serial.println("Error setting my mDNS name");
  } else {
    Serial.println("mDNS started with name " + String(myname));
  }
  MDNS.addService("http", "tcp", 80);
}

void ota_init() {
  ArduinoOTA.onStart([]() {
    Serial.println("Start");
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
    ESP.restart();
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("OTA updating enabled");
}

void config_default() {
  memset(&config, 0, sizeof(config));
  config.offset = -14401; // Default to EST
}

// Load settings from EEPROM
void config_load() {
  uint32_t chipid;
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.get(0, chipid);
  if (chipid == ESP.getChipId()) {
    EEPROM.get(0 + sizeof(chipid), config);
    Serial.println("Loading saved configuration from EEPROM");
  } else {
    config_default();
  }
  EEPROM.end();
}

// Save settings to EEPROM
void config_save() {
  uint32_t chipid = ESP.getChipId();
  EEPROM.begin(EEPROM_SIZE);
  EEPROM.put(0, chipid);
  EEPROM.put(0 + sizeof(chipid), config);
  EEPROM.commit();
  EEPROM.end();
  Serial.println("Saved config of size " + String(sizeof(config) + sizeof(chipid)));
}

// Send all config as JSON
void sendAll()
{
  String json = "{";
  char buf[10];
  time_t tztime = tzp ? tzp->toLocal(timeClient.getEpochTime()) : timeClient.getEpochTime();

  json += "\"hour\": \"" + String(hour(tztime)) + "\",";
  json += "\"minute\": \"" + String(minute(tztime)) + "\",";
  json += "\"offset\": \"" + String(config.offset) + "\",";
  json += "\"r_brightness\": \"" + String(config.r_brightness) + "\",";
  json += "\"g_brightness\": \"" + String(config.g_brightness) + "\",";
  json += "\"b_brightness\": \"" + String(config.b_brightness) + "\",";
  json += "\"name\": \"" + String(config.name) + "\",";
  json += "\"bversion\": \"" + String(INO_VERSION) + "\"";
  json += "}";
  server.send(200, "text/json", json);
}

void httpError(String error) {
  server.send(400, "text/json", "{\"error\": \"" + error + "\"}");
}

void saveOffset()
{
  int newOffset = server.arg("offset").toInt();
  if (newOffset != config.offset) {
    config.offset = newOffset;
    config_save();
    update_internal_offset(newOffset);
    Serial.println("Offset: " + String(config.offset));
  }
  server.send(204, "text/json");
}

void saveName()
{
  String newName = server.arg("name");
  if (newName.length() >= sizeof(config.name) - 1) {
    httpError("Name is too long");
    return;
  }
  if (newName != String(config.name)) {
    strncpy(config.name, newName.c_str(), sizeof(config.name));
    config_save();
    MDNS.setInstanceName(newName);
    Serial.println("Name: " + String(config.name));
  }
  server.send(204, "text/json");
}

void saveBrightness()
{
  int r_brightness = server.arg("r_brightness").toInt();
  int g_brightness = server.arg("g_brightness").toInt();
  int b_brightness = server.arg("b_brightness").toInt();
  if (r_brightness != config.r_brightness || 
      g_brightness != config.g_brightness ||
      b_brightness != config.b_brightness) {
    config.r_brightness = r_brightness;
    config.g_brightness = g_brightness;
    config.b_brightness = b_brightness;
    config_save();
  }
  server.send(204, "text/json");
}

void systemPost()
{
   String clearConfig = server.arg("clear_config");
   if (clearConfig == "on") {
    config_default();
    config_save();
   }
   String reset = server.arg("reboot");
   if (reset == "on") {
    web_reset = true;
   }
   server.send(204, "text/json");
}

// Setup handlers and start the web server
void webserver_init() {
  server.serveStatic("/", SPIFFS, "/index.html");
  server.serveStatic("/favicon.ico", SPIFFS, "/favicon.ico", "max-age=86400");
  server.serveStatic("/fonts", SPIFFS, "/fonts", "max-age=86400");
  server.serveStatic("/js", SPIFFS, "/js", "max-age=86400");
  server.serveStatic("/css", SPIFFS, "/css", "max-age=86400");
  server.serveStatic("/images", SPIFFS, "/images", "max-age=86400");
  server.on("/r/all", HTTP_GET, sendAll);
  server.on("/r/offset", HTTP_POST, saveOffset);
  server.on("/r/name", HTTP_POST, saveName);
  server.on("/r/brightness", HTTP_POST, saveBrightness);
  server.on("/r/system", HTTP_POST, systemPost);
  server.onNotFound([]() {
    String message = "File Not Found: " + server.uri() + "\n\n";
    message += "Be sure to populate the SPIFFS data!\n\n";
    message += "(firmware version " + String(INO_VERSION) + ")";
    server.send ( 404, "text/plain", message );
  });
  server.begin();
  Serial.println("Web Server started");
}

// Blink the LEDs per the current time
void blink_cb() {
  static bool toggle = HIGH;
  static State state = NONE;
  static int hours, tens, mins, cnt = 0;
  time_t tztime = tzp ? tzp->toLocal(timeClient.getEpochTime()) : timeClient.getEpochTime();
  if (state == NONE) {
    hours = hour(tztime);
    hours = ((hours / 13) + (hours % 13)) ?: 12; // Convert 24h to 12h
    mins = minute(tztime);
    tens = mins / 10;
    mins = mins % 10;
    state = HOURS;
  }
  if (toggle && cnt++ >= 0) {
    if (state == HOURS) {
      if (cnt > hours) {
        state = TENS;
        cnt = -1;
      } else {
        analogWrite(LED_RED, config.r_brightness);
      }
    }
    else if (state == TENS) {
      if (cnt > tens) {
        state = MINS;
        cnt = -1;
      } else {
        analogWrite(LED_GRN, config.g_brightness);
      }
    }
    else if (state == MINS) {
      if (cnt > mins) {
        state = NONE;
        cnt = -2;
      } else {
        analogWrite(LED_BLU, config.b_brightness);
      }
    }
  } else {
    analogWrite(LED_RED, 0);
    analogWrite(LED_GRN, 0);
    analogWrite(LED_BLU, 0);
  }
  toggle = !toggle;
}

Task blinker(400, TASK_FOREVER, &blink_cb, &runner, false);

void setup() {
  Serial.begin(115200);
  pinMode(LED_RED, OUTPUT);
  pinMode(LED_GRN, OUTPUT);
  pinMode(LED_BLU, OUTPUT);
  pinMode(LED_TINY_BLU, OUTPUT);

  // Turn off tiny blu light
  digitalWrite(LED_TINY_BLU, HIGH);

  ticker.attach(0.5, tick);
  wifi_init();
  ticker.attach(0.5, tick);
  config_load();
  time_init();
  SPIFFS.begin();
  webserver_init();
  mdns_init();
  ota_init();
  ticker.detach();

  ESP.wdtEnable(5000);

  blinker.enable();
  runner.startNow();
  Serial.println("Startup complete!");
}

void loop() {
  static int lastButtonPressStart = 0;
  MDNS.update();
  timeClient.update();
  server.handleClient();
  runner.execute();
  ArduinoOTA.handle();
  if (digitalRead(INPUT_BUTTON) == LOW) {
    // Button is pressed
    if (lastButtonPressStart == 0) {
      lastButtonPressStart = timeClient.getEpochTime();
    }
  } else if (lastButtonPressStart > 0) {
    // Button has been released
    int pressDuration = timeClient.getEpochTime() - lastButtonPressStart;
    if (pressDuration > 9) {
      Serial.println("Resetting CONFIG due to SUPER long button press");
      config_default();
      config_save();
    } else if (pressDuration > 4) {
      Serial.println("Resetting WIFI due to long button press");
      wifiManager.resetSettings();
    } else if (pressDuration > 1) {
      Serial.println("Resetting device due to short button press");
      ESP.restart();
    } else {
      Serial.println("Forcing time update due to button press");
      timeClient.forceUpdate();
    }
    lastButtonPressStart = 0;
  }
  if (web_reset) {
    Serial.println("Resetting device due to web request");
    ESP.restart();
  }
}




