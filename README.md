# ESP8266 Blinking clock #

Turn the $5 Witty Cloud Development (ESP8266-12E) board into clock that indicates time by blinking.

![witty.jpg](https://bitbucket.org/repo/6LKdbj/images/808783627-witty.jpg)

## How it works 

The Blink Clock blinks the three available LEDs to indicate the current time.

 * The RED LED indicates the hour
 * The GREEN LED indicates the tens digit of the minutes
 * The BLUE LED indicats the last digit of the minutes

For example, suppose the time is 3:47:

 * The RED LED will blink three times followed by a short pause
 * The GREEN LED will blink four times followed by a short pause
 * The BLUE LED will blink seven times followed by a long pause
 * Repeat..

## Features

 * Easy to read without glasses
 * Adjustable brightness (Automatic coming soon!)
 * Easy setup and control via WiFi
 * HTML5 Responsive Web Interface works on all devices
 * Programmable name for easy identification
 * Configurable timezone

## Uses

 * Tell time in the middle of the night without finding your glasses

### 

Checkout the [wiki](https://bitbucket.org/polandj/blinkclock/wiki/Home) to get started!